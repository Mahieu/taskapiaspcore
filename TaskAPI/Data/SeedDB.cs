﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskAPI.Models;

namespace TaskAPI.Data
{
    public class SeedDB
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<TaskContext>();
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            context.Database.EnsureCreated();
            if (!context.Users.Any())
            {
                ApplicationUser user = new ApplicationUser()
                {
                    Email = "nicolas.mahieu.isf@gmail.com",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserName = "nicolas.mahieu.isf@gmail.com",
                    EmailConfirmed = true
                };
                userManager.CreateAsync(user, "Halo7810!");
            }
        }
    }
}
