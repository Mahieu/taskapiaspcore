﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskAPI.Models;

namespace TaskAPI.Data
{
    public class TaskContext : IdentityDbContext<ApplicationUser>
    {
        public TaskContext(DbContextOptions<TaskContext> options) : base(options) {
        
        }
        public DbSet<TaskItem> TaskItems { get; set; }
    }
}
