﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskAPI.Models
{
    public class ApplicationUser : IdentityUser
    {
        public ICollection<TaskItem> TaskItems { get; set; }
    }
}
