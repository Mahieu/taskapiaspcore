﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TaskAPI.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Email requis.")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Mot de passe requis.")]
        public string Password { get; set; }
    }
}
