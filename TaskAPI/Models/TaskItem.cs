﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace TaskAPI.Models
{
    public class TaskItem
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Le titre ne peut être vide")]
        public string Title { get; set; }
        [Required(ErrorMessage = "La decription ne peut être vide")]
        public string Description { get; set; }
        [Required(ErrorMessage = "La date de fin ne peut être vide")]
        public DateTime DueDate { get; set; }
        public int Progress { get; set; }
        public string UserId { get; set; }
        [JsonIgnore]
        public ApplicationUser User { get; set; }
        public TaskItem()
        {
            this.Progress = 0;
        }
    }

}
